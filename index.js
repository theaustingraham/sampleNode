'use strict';

// required to setup routes etc...
const Hapi = require('hapi');
// required to write to the file system
const fs = require('fs');

const server = new Hapi.Server();
server.connection({ port: 8888 });

server.register(require('inert'), (err) => {

    if (err) {
        throw err;
    }
    
    // load the main view
    server.route({
        method: 'GET',
        path: '/',
        handler: function (request, reply) {
            reply.file('./html/form.html');
        }
    });

    // to bring in assets like the main js
    server.route({
        method: 'GET',
        path: '/js/{name}',
        handler: function (request, reply) {
            reply.file('./js/'+request.params.name);
        }
    });
});

// Handles the ajax post or put
server.route({
    method: ['PUT', 'POST'],
    path: '/api/get/',
    handler: function (request, reply) {
    	console.log('POST SUCCESS');
        console.log(request.payload);
    	// Reply data for ajax to use
    	reply({
            statusCode: 200,
            message: 'Getting All Form Data',
            response: [request.payload]
        });
        // Write data to a file ....
        var stream = fs.createWriteStream('tmp/' + request.payload.myname + Math.floor((Math.random() * 1000) + 1) + ".txt");
		stream.once('open', function(fd) {
		  stream.write("Name:" + request.payload.myname +"\n");
		  stream.write("Email:" + request.payload.myemail +"\n");
		  stream.write("Food:" + request.payload.myfood +"\n");
		  stream.end();
		});									
        
    }
});


// handles server failing to start
server.start((err) => {

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});