/* AG - TEST */
(function($){
    /**
     * Main sample Node
     */
    sampleNode = {};
    sampleNode.Global = {
        version: 1, 

        init: function() {
            sampleNode.Global.setupBindings(); 
        },

        setupBindings: function() {
            
            $('#mainForm').submit(function(event) {
               event.preventDefault();

               $.ajax({
                   url: '/api/get/',
                   type: 'POST',
                   dataType: 'json',
                   data: $('#mainForm').serialize(),
               })
               .done(function(data) {
                   console.log("Got your ajax post - utilizing data......");
                   sampleNode.Global.ajaxDataHandler(data);
               })
               .fail(function(data) {
                   console.log("You got an error.....");
                   sampleNode.Global.ajaxDataHandler(data);
               })
               .always(function(data) {
                   console.log("complete......");
               });
               

            });
        },

        ajaxDataHandler: function(data) {
            console.log(data.response[0]);
            sampleNode.Global.renderResponse(data.response[0]);
        },

        renderResponse: function(data){
            $('#content').html('<h2>Your Submitted Data:</h2>' + 'Name:' + data.myname + '<br>' + 'Email:' + data.myemail + '<br>' + 'Favorite Food:' + data.myfood);
        }
    }
sampleNode.Global.init(); 
}(jQuery));